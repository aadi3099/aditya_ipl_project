let fs = require('fs')
let csv=require('csvtojson')
let cs = require('csvtojson')
const deliveriesPath = '../data/deliveries.csv';
const matchesPath = '../data/matches.csv';
csv()
.fromFile(deliveriesPath)
.then((deliveriesArray)=>{
    return csv()
.fromFile(matchesPath)
.then((matchesArray) => {
    function extraRunsPerTeam(deliveriesArray, matchesArray)
    {
        let extraRun = {};
        for(let i = 0; i < matchesArray.length; i++)
        {
            if(matchesArray[i].season == '2016')
            {
                for(let j = 0; j < deliveriesArray.length; j++)
                {
                    if(deliveriesArray[j].match_id == matchesArray[i].id)
                    {
                        let data = parseInt((deliveriesArray[j].extra_runs),10);
                        if(extraRun[deliveriesArray[j].bowling_team])
                        {
                            extraRun[deliveriesArray[j].bowling_team] += data;
                        }
                        else
                        {
                            extraRun[deliveriesArray[j].bowling_team] = data;
                        }
                    }
                }
            }
        }
        console.log(extraRun);
        let data = JSON.stringify(extraRun, null, 2);
        fs.writeFile('../output/extraRuns.json', data, function (err) 
        {
            if (err) 
            {
                return console.log(err);
            }
            console.log('Written to the file.');
        })
    }
extraRunsPerTeam(deliveriesArray, matchesArray);
})
})
.catch((err) => 
{
    console.error(err);
})