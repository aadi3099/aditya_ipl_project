let csv = require('csvtojson')
let fs = require('fs')
const csvFilePath='../data/matches.csv'
csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    jsonObj;

function matchesWonPerYear(jsonObj)
{
    let obj = {}
    for(let val of jsonObj)
    {   
        let year = val["season"];
        let win = val["winner"];
        if(obj[year])
        {
            if(obj[year][win])
            {
                obj[year][win] +=1;
            }
            else
            {
                obj[year][win] = 1;
            }
        }
        else
        {
            obj[year] = {};
            obj[year][win] = 1;
        }
    }
    console.log(obj);
    let data = JSON.stringify(obj, null ,2);
    fs.writeFile('../output/matchesPerYear.json', data, function (err) {
      if (err) 
      {
        return console.log(err);
      }
      console.log('Written to the file.');
    })
}
matchesWonPerYear(jsonObj);
})
.catch((err) => 
{
    console.error("There is an error in the file.");
})