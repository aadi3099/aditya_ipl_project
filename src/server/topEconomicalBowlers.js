let fs = require('fs')
let csv=require('csvtojson')
let cs = require('csvtojson');
const { parse } = require('path');
const deliveriesPath = '../data/deliveries.csv';
const matchesPath = '../data/matches.csv';
csv()
.fromFile(deliveriesPath)
.then((deliveriesArray)=>{
    return csv()
.fromFile(matchesPath)
.then((matchesArray) => {

    function topEconomicalBowlers(matchesArray, deliveriesArray)
    {
        let obj = {};
        for(let i = 0; i < matchesArray.length; i++)
        {
            if(matchesArray[i].season == '2015')
            {
                for(let j = 0; j < deliveriesArray.length; j++)
                {
                    if(matchesArray[i].id == deliveriesArray[j].match_id)
                    {
                        if(obj[deliveriesArray[j].bowler])
                        {
                            obj[deliveriesArray[j].bowler][0] += parseInt(deliveriesArray[j].total_runs,10);
                            obj[deliveriesArray[j].bowler][1] += 1;
                        }
                        else
                        {
                            obj[deliveriesArray[j].bowler] = [];    
                            obj[deliveriesArray[j].bowler].push(parseInt(deliveriesArray[j].total_runs,10), 1);
                        }
                    }
                }
            }
        }
        let arr = [];
        for(let val in obj)
        {
            let data = (obj[val][0]) * 6 / obj[val][1];
            arr.push([val, data]);
        }
        arr.sort((a, b) => {
            return a[1] - b[1]; 
        })
        let i = 0;
        let economyBowler = {};
        while(i < 10)
        {
            economyBowler[arr[i][0]] = arr[i][1];
            i++;
        }
        console.log(economyBowler);
        let value = JSON.stringify(obj, null ,2);
        fs.writeFile('../output/economicalBowler.json', value, function (err) 
        {
            if (err) 
            {
                return console.log(err);
            }
            console.log('Written to the file.');
        })
    }
    topEconomicalBowlers(matchesArray, deliveriesArray);
})
})
.catch((err) => 
{
    console.error("There is an error in the file.");
})